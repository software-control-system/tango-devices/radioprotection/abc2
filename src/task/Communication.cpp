// ============================================================================
//
// = CONTEXT
//    TANGO Project - Communication Class
//
// = FILENAME
//    Communication.cpp
//
// = AUTHOR
//    X. Elattaoui
// ============================================================================

#include <yat/threading/Utilities.h> //- sleep
#include <yat/utils/XString.h>       //- to_string
#include <yat4tango/ExceptionHelper.h>   //- macro : TANGO_EXCEPTION_TO_LOG_STREAM
#include "Communication.hpp"

namespace ABC2_ns
{
// ============================================================================
// Communication::Communication
// ============================================================================
Communication::Communication (Tango::DeviceImpl * host_device,
                              std::string com_device_name)
: yat4tango::TangoLogAdapter(host_device),
  m_proxy (0),
  m_host_dev(host_device),
  m_dev_name(com_device_name),
  m_error(""),
  m_com_state(Tango::INIT)
{
  //- Noop
}

// ============================================================================
// Communication::~Communication
// ============================================================================
Communication::~Communication (void)
{
  delete_proxy();
}

// ============================================================================
// Communication:create_proxy
// ============================================================================
void Communication::create_proxy()
{
  if ( !m_dev_name.empty() )
  {
    try
    {
      //- create proxy
      m_proxy.reset(new Tango::DeviceProxyHelper(m_dev_name, m_host_dev));
      m_com_state = m_proxy->get_device_proxy()->state();
    }
    catch(Tango::DevFailed& df)
    {
      m_com_state = Tango::FAULT;
      TANGO_EXCEPTION_TO_LOG_STREAM(df);
      delete_proxy();
      return;
    }
  }
}

// ============================================================================
// Communication::delete_proxy
// ============================================================================
void Communication::delete_proxy()
{
  m_proxy.reset();
}

// ============================================================================
// Communication::check_proxy
// ============================================================================
void Communication::check_proxy()
{
  if ( m_proxy )
  {
    try
    {
      //- check device is up !
      m_proxy->get_device_proxy()->ping();
    }
    catch(Tango::DevFailed& df)
    {
      m_com_state = Tango::ALARM;
      //- store the Tango error
      m_error = std::string(df.errors[0].desc);
      ERROR_STREAM << m_error << std::endl;
      throw;
    }
    
    try
    {
      //- check device is up !
      m_com_state = m_proxy->get_device_proxy()->state();
      if( m_com_state != Tango::OPEN )
      {
        m_com_state = Tango::ALARM;
      	m_error = "Cannot read back state of device \"" + m_dev_name + "\" or its Tango state is not OPEN.\n";
      }
    }
    catch(Tango::DevFailed& df)
    {
      m_com_state = Tango::ALARM;
      TANGO_EXCEPTION_TO_LOG_STREAM(df);
      throw;
    }
  }
}

// ============================================================================
// Communication::read_input_registers
// ============================================================================
std::vector<short> Communication::read_input_registers(short reg_addr, short reg_lgth)
{
  std::vector<short> vargout;

  if ( !m_proxy )
  {
    {
      m_com_state = Tango::FAULT;
      m_error = "Communication link proxy is not created.";
    }
    Tango::Except::throw_exception("COMMUNICATION_ERROR",
                                   m_error,
                                   "Communication::read_input_registers");
  }

  try
  {
    vargout.clear();
    check_proxy();
  }
  catch(...)
  {
    throw;
  }

  try
  {
    if ( m_proxy )
    {
      DEBUG_STREAM << "Communication::read_input_registers -> for register add *" << reg_addr << "*" << std::endl;
      std::vector<short> vargin;
      vargin.push_back(reg_addr);
      vargin.push_back(reg_lgth);

      Tango::DeviceData ddin;
      ddin << vargin;

      //- send the cmd 
      m_proxy->command_inout("ReadInputRegisters", vargin, vargout);
    }
  }
  catch(Tango::DevFailed & df)
  {
    m_com_state = Tango::ALARM;
    error("Communication::read_input_registers received a DevFailed exception.");
    TANGO_EXCEPTION_TO_LOG_STREAM(df);
    throw df;
  }
  catch(...)
  {
    m_com_state = Tango::ALARM;
    error("Communication::read_input_registers received a [...] exception.");
    throw;
  }
 
  return vargout;
}

// ============================================================================
// Communication::read_holding_registers
// ============================================================================
std::vector<short> Communication::read_holding_registers(short reg_addr, short reg_lgth)
{
  std::vector<short> vargout;

  if ( !m_proxy )
  {
    {
      m_com_state = Tango::FAULT;
      m_error = "Communication link proxy is not created.";
    }
    Tango::Except::throw_exception("COMMUNICATION_ERROR",
                                   m_error,
                                   "Communication::read_holding_registers");
  }

  try
  {
    vargout.clear();
    check_proxy();
  }
  catch(...)
  {
    return vargout;
  }

  try
  {
    if ( m_proxy )
    {       
      std::vector<short> vargin;
      vargin.push_back(reg_addr);
      vargin.push_back(reg_lgth);

      Tango::DeviceData ddin;
      ddin << vargin;

      //- send the cmd 
      m_proxy->command_inout("ReadHoldingRegisters", vargin, vargout);
    }
  }
  catch(Tango::DevFailed & df)
  {
    m_com_state = Tango::FAULT;
    error("Communication::read_holding_registers received a DevFailed exception");
    TANGO_EXCEPTION_TO_LOG_STREAM(df);
  }
  catch(...)
  {
    m_com_state = Tango::FAULT;
    error("Communication::read_holding_registers received a [...] exception.");
  }
  
  return vargout;
}

// ============================================================================
// Communication::write_holding_register
// ============================================================================
void Communication::write_holding_register(short reg_addr, short val)
{
  if ( !m_proxy )
  {
    {
      m_com_state = Tango::FAULT;
      m_error = "Communication link proxy is not created.";
    }
    Tango::Except::throw_exception("COMMUNICATION_ERROR",
                                   m_error,
                                   "Communication::write_holding_register");
  }

  try
  {
    check_proxy();
  }
  catch(...)
  {
    throw;
  }

  try
  {
    if ( m_proxy )
    {
      DEBUG_STREAM << "Communication::write_holding_register -> for register add *" << reg_addr << "*" << std::endl;
      std::vector<short> vargin;
      vargin.push_back(reg_addr);
      vargin.push_back(val);

      Tango::DeviceData ddin, ddout;
      ddin << vargin;

      //- send the cmd 
      m_proxy->command_in("PresetSingleRegister", vargin);  
    }
  }
  catch(Tango::DevFailed & df)
  {
    m_com_state = Tango::ALARM;
    error("Communication::write_holding_register received a DevFailed exception.");
    TANGO_EXCEPTION_TO_LOG_STREAM(df);
    throw df;
  }
  catch(...)
  {
    m_com_state = Tango::ALARM;
    error("Communication::write_holding_register received a [...] exception.");
    throw;
  }
}

// ============================================================================
// Communication::clear_error
// ============================================================================
void Communication::clear_error()
{
  m_com_state = Tango::ON;
  m_error.clear();
}

} //- end namespace
