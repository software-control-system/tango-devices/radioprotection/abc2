// ============================================================================
//
// = CONTEXT
//    TANGO Project - ABC2 Data Class
//
// = FILENAME
//    ABC2Data.hpp
//
// = AUTHOR
//    X. Elattaoui
// ============================================================================

#ifndef _ABC2_DATA_H_
#define _ABC2_DATA_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>
#include <yat/Portability.h>


/**
 *  \brief ABC2 Data
 *
 *  \author Xavier Elattaoui
 *  \date 07-2022
 */

 /**
 *  HoldingRegisters struct description:
 *    Read/Write data types.
 */

struct HoldingRegisters
{
public:

  //- ctor
  HoldingRegisters ()
  {
    reset();
  }

  //- copy ctor
  HoldingRegisters (const HoldingRegisters& src)
  {
    *this = src;
  }

  //- operator=
  const HoldingRegisters& operator= (const HoldingRegisters& src)
  {
    if (this != &src)
    {
      dose_alarm_mode         = src.dose_alarm_mode;
      dose_alarm_level        = src.dose_alarm_level;
      dose_alarm_muted        = src.dose_alarm_muted;
      dose_reset              = src.dose_reset;
      measurement_number      = src.measurement_number;
      current_time            = src.current_time;
      dev_calibration_factor  = src.dev_calibration_factor;
      vial_calibration_factor = src.vial_calibration_factor;
      temperature_threshold   = src.temperature_threshold;
    }

    return *this;
  }

  //- reset
  void reset ()
  {
    dose_alarm_mode.clear();
    dose_alarm_level        = 0.;
    dose_alarm_muted        = 0;
    dose_reset              = 0;
    measurement_number      = 0;
    current_time.clear();
    dev_calibration_factor  = 0;
    vial_calibration_factor = 0;
    temperature_threshold   = 0;
  }

  //- in case of error, invalid data!
  void invalid_data ()
  {
    reset();
  }

  //- data
  //- 0 : alarm off, 1 : alarm on, 2 : alarm on + external
  std::string                 dose_alarm_mode;
  //- in nSv
  double                      dose_alarm_level;
  //- always 0
  unsigned short              not_used;
  //- 0 : no, >=1 yes
  unsigned short              dose_alarm_muted;
  //- >0 : dose should be resetted, 0: dose just resetted
  unsigned short              dose_reset;
  //- 1: Increase measurement number by 1., 0: it is done
  unsigned short              measurement_number;
  //- current time
  std::string                 current_time;
  //- device calibration factor: default 100
  unsigned short              dev_calibration_factor;
  //- vial calibration factor: default 127
  unsigned short              vial_calibration_factor;
  //- temp threshold
  unsigned short              temperature_threshold;

};

 /**
 *  InputRegisters struct description:
 *    This struct shares ABC2 data between device and thread.
 */

struct InputRegisters
{
public:

  //- ctor
  InputRegisters ()
  {
    reset();
  }

  //- copy ctor
  InputRegisters (const InputRegisters& src)
  {
    *this = src;
  }

  //- operator=
  const InputRegisters& operator= (const InputRegisters& src)
  {
    if (this != &src)
    {
      firmware_version            = src.firmware_version;
      serial_number               = src.serial_number;
      vial_type                   = src.vial_type;
      vial_serial                 = src.vial_serial;
      calibration                 = src.calibration;
      initial_loading             = src.initial_loading;
      drops_at_test_start         = src.drops_at_test_start;
      start_time                  = src.start_time;
      temperature                 = src.temperature;
      battery_charge              = src.battery_charge;
      measurement_number          = src.measurement_number;
      total_dose                  = src.total_dose;
      dose_since_reset            = src.dose_since_reset;
      total_pops                  = src.total_pops;
      pops_since_reset            = src.pops_since_reset;
      remaing_drops               = src.remaing_drops;
      external_alarm              = src.external_alarm;
      dose_alarm_tripped          = src.dose_alarm_tripped;
      dev_calibration_factor_r    = src.dev_calibration_factor_r;
      vial_calibration_factor_r   = src.vial_calibration_factor_r;
      last_reset_date             = src.last_reset_date;
      last_vial_reset_date        = src.last_vial_reset_date;
      display_version             = src.display_version;
      temperature_warning_state   = src.temperature_warning_state;
      vial_alarm_state            = src.vial_alarm_state;
      time_since_reset            = src.time_since_reset;
      timestamp                   = src.timestamp;
      on_error                    = src.on_error;
    }
    return *this;
  }

  //- reset
  void reset ()
  {
    firmware_version          = "";
    serial_number             = 0;
    vial_type                 = "";
    vial_serial               = "";
    calibration               = 0;
    initial_loading           = 0;
    drops_at_test_start       = 0;
    start_time.clear();
    temperature               = 0;
    battery_charge            = 0;
    measurement_number        = 0;
    total_dose                = 0;
    dose_since_reset          = 0;
    total_pops                = 0;
    pops_since_reset          = 0;
    remaing_drops             = 0;
    external_alarm            = 0;
    dose_alarm_tripped        = 0;
    dev_calibration_factor_r  = 100;
    vial_calibration_factor_r = 127;
    last_reset_date           = "Command not sent";
    last_vial_reset_date.clear();
    display_version.clear();
    temperature_warning_state = 0;
    vial_alarm_state          = 0;
    time_since_reset          = "";
    timestamp = "";
    on_error  = false; //- true at startup!
  }

  //- in case of error, invalid data!
  void invalid_data ()
  {
    reset();
  }

  //- data
  //- Firmware Version (for example: 'RS1.30')
  std::string firmware_version;
  //- Serial number (for example '00049')
  unsigned long serial_number;
  //- vial type code (0: no vial, 1: "SDD-100" )
  std::string vial_type;
  //- 2x4 digits of vial serial (for example:'SDD-0123-4567'-> 0123, 4567)
  std::string vial_serial;
  //-  device calibration factor * vial calibration factor / 100
  unsigned long calibration;
  //- initial number of bubbles
  unsigned short initial_loading;
  //- Number of drops at test start
  unsigned short drops_at_test_start;
  //- start time of measurement (7 chars: year, month, day, hour, minutes, seconds, centiseconds)
  std::string start_time;
  //- temperature in Celsius * 100
  unsigned short temperature;
  //- % charge remaining (0-100, 255-> AC)
  unsigned short battery_charge;
  //- Measurement number
  unsigned short measurement_number;
  //- Dose since the beginning of history (in nSv)
  double total_dose;
  //- Dose since reset (in nSv)
  double dose_since_reset;
  //- Total number of pops (counts)
  unsigned short total_pops;
  //- Number of pops since dose reset (count)
  unsigned short pops_since_reset;
  //- Number of drops remaining in vial
  unsigned short remaing_drops;
  //- External alarm firing state (0: no, >=1: yes)
  unsigned short external_alarm;
  //- Dose alarm tripped (0: no, >=1: yes)
  unsigned short dose_alarm_tripped;
  //- range: 0 - 200, default: 100
  unsigned short dev_calibration_factor_r;
  //- range: 0 - 255, default: 127
  unsigned short vial_calibration_factor_r;
  //- last reset of device: manual or auto (7 char: year, month, day, hour, minutes, seconds, centiseconds)
  std::string last_reset_date;
  //- date of vial reset
  std::string last_vial_reset_date;
  //- display firmware version (eg. 1.4)
  std::string display_version;
  //- The state of the temperature alarm (0: temperature is below the temperature alarm threshold, 1: temp is too high)
  unsigned short temperature_warning_state;
  //- 3 warning levels based on "Remaining drops/Initial loading" rate. 0 (Alarm off): >66%, 1 (warning Level 1): 20-66%, 2 (warning level 2): <20%
  unsigned short vial_alarm_state;
  //- time since reset cpt
  std::string time_since_reset;
  //- timestamp
  std::string timestamp;
  //- bool to reset data on coms errors
  bool on_error;

};

#endif // _ABC2_DATA_H_
