// ============================================================================
//
// = CONTEXT
//    TANGO Project - Task to gather Neutron monitor data
//
// = File
//    ABC2Task.cpp
//
// = AUTHOR
//    X. Elattaoui - SOLEIL
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
// #include <tango.h>
#include <math.h>
#include <yat/threading/Mutex.h>
#include <yat/time/Time.h>
#include <yat/utils/String.h>   //- to lower/upper
#include <yat/utils/XString.h>  //- to_num
#include "ABC2Task.hpp"
#include "Registers.h"

//-----------------------------------------------------------------------------
//- task period
const std::size_t PERIODIC_MSG_PERIOD = 1000; //- in ms
// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================
const std::size_t kRESET_CMD          = (yat::FIRST_USER_MSG + 10);
const std::size_t kUNLOCK_CMDs_MSG    = (yat::FIRST_USER_MSG + 1000);

const std::size_t kLOCK_CMDS_AFTER    = 15000;        //- lock cmds after 15s
const double k4_HOURS_RESET_VALUE     = (3600. * 4.); //- convert 4h in seconds

//- dose alarm mode
typedef enum {
  ALARM_OFF = 0,
  ALARM_ON,
  ALARM_ON_EXT
} dose_alarm_mode_states;

namespace ABC2_ns
{
// ======================================================================
// ABC2Task::ABC2Task
// ======================================================================
ABC2Task::ABC2Task (Tango::DeviceImpl* host_device,
                    std::string ds_proxy_name,
                    std::size_t nb_retries)
  : yat4tango::DeviceTask(host_device),
    m_reset_cmd_unlocked(false),
    m_pwd_in_clear(""),
    m_pwd_decrypted(""),
    m_kn(0),
    m_kd(0),
    m_last_reset_date("Command not sent"),
    m_host_device(host_device),
    m_proxy_device_name(ds_proxy_name),
    m_com_p(0),
    m_abc2_inp_reg(),
    m_abc2_hold_reg(),
    m_battery_charge(""),
    m_err_cpt(0),
    m_reset_err_cpt(0),
    m_nb_max_retries(nb_retries),
    m_nb_max_reset_retries(nb_retries)
{
  m_codes_list.clear();

  m_abc2_inp_reg.reset();
  m_abc2_hold_reg.reset();
}

// ======================================================================
// ABC2Task::~ABC2Task
// ======================================================================
ABC2Task::~ABC2Task (void)
{

}

// ============================================================================
// ABC2Task::process_message
// ============================================================================
void ABC2Task::process_message (yat::Message& _msg) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "ABC2Task::handle_message::receiving msg " << _msg.to_string() << std::endl;

  //- handle msg
  switch (_msg.type())
  {
  //- THREAD_INIT ----------------------
  case yat::TASK_INIT:
    {
      DEBUG_STREAM << "ABC2Task::handle_message::THREAD_INIT::thread is starting up" << std::endl;

      //- "initialization" code goes here
      //----------------------------------------------------
      yat::Timer t;

      //- create communication proxy
      init_communication();

      //- initialize reset timer : find time (in seconds) to the next reset
      initialize_4h_reset_timer();

      //- update holding registers data
      update_holding_registers();

      //- update input registers data
      update_input_registers();

      //- configure optional msg handling
      set_periodic_msg_period(PERIODIC_MSG_PERIOD);
      enable_timeout_msg(false);
      enable_periodic_msg(true);
      INFO_STREAM << "\tABC2Task::TASK_INIT finished in " << t.elapsed_msec() << " ms." << std::endl;
    }
    break;
  //- THREAD_EXIT ----------------------
  case yat::TASK_EXIT:
    {
      DEBUG_STREAM << "\tABC2Task::handle_message::THREAD_EXIT::thread is quitting" << std::endl;

      //- "release" code goes here
      //----------------------------------------------------
      try
      {
        close_communication();
      }
      catch(...)
      {
        //- Noop
      }
    }
    break;
  //- THREAD_PERIODIC ------------------
  case yat::TASK_PERIODIC:
    {
      //- code relative to the task's periodic job goes here
      //----------------------------------------------------
      yat::Timer t;

      //- check if reset cmd has been unlocked
      if ( m_reset_cmd_unlocked )
      {
        //- if time elapsed, lock the reset cmd
        if ( m_unlock_cmd_timer.elapsed_msec() > kLOCK_CMDS_AFTER )
          m_reset_cmd_unlocked = false;
      }

      //- check if it is time to send reset (4h timer as expired ?)
      //- or if last reset command failed!!
      if ( m_automatic_reset_timer.expired() || m_reset_err_cpt )
      {
        //- send reset cmd
        reset_i();
        //- re-initialize the 4h counter for another period (4h)
        initialize_4h_reset_timer();
      }
      else
      {
        //- update ABC data
        update_input_registers();

        //- read back holding registers
        update_holding_registers();
        
        // //- build timestamp
        // yat::Timestamp startup_ts;
        // _GET_TIME(startup_ts);

        // {//- copy in critical section
        //   yat::AutoMutex<> guard(m_input_reg_mutex);
        //   _TIMESTAMP_TO_DATE(startup_ts, m_abc2_inp_reg.timestamp);
        //   //- convert time_since_reset seconds in hour, minutes, seconds format
        //   yat::Duration tsr(m_automatic_reset_timer.time_to_expiration());
        //   m_abc2_inp_reg.time_since_reset = tsr.to_string();
        // }
      }
    }
    break;
  //- TIMEOUT -------------------
  case yat::TASK_TIMEOUT:
    {
      //- code relative to the task's tmo handling goes here
    }
    break;
  //- UNLOCK CMD MSG -----------------
  case kUNLOCK_CMDs_MSG:
    {
      //- decode crypted list of codes
      decrypt_codes();

      //- compare passwords and unlock cmds if OK
      unlock_cmds_i();

      //- restart timer
      if ( m_reset_cmd_unlocked )
      {
        m_unlock_cmd_timer.restart();
      }
    }
    break;
  //- RESET MSG -----------------
  case kRESET_CMD:
    {
      //- check the command has been unlocked
      if ( m_reset_cmd_unlocked )
      {
        //- reset the neutron monitor
        reset_i();
      }
    }
    break;
  //- UNHANDLED MSG --------------------
  default:
    FATAL_STREAM << "ABC2Task::handle_message::unhandled msg type received" << std::endl;
    break;
  }
}

// ============================================================================
// ABC2Task::get_holding_registers_data
// ============================================================================
HoldingRegisters ABC2Task::get_holding_registers_data ()
{
  yat::AutoMutex<> guard(m_holding_reg_mutex);

  return m_abc2_hold_reg;
}

// ============================================================================
// ABC2Task::update_holding_registers
// ============================================================================
void ABC2Task::update_holding_registers()
{
  std::vector<short> vholding_reg_data;
  HoldingRegisters tmp_abc2_hold_reg;

  try
  {
    if(m_com_p)
    {
      //- read back all data in one shot
      vholding_reg_data.clear();
      vholding_reg_data = m_com_p->read_holding_registers(HOLDING_REGISTERS_START_ADDRESS, HOLDING_REGISTERS_LENGTH);
    }
    //- parse data:
    if(vholding_reg_data.size() == HOLDING_REGISTERS_LENGTH)
    {
      //- dose alarm mode
      uint d_alarm_mode = vholding_reg_data[HREG_DOSE_ALARM_MODE_IDX];
      if(d_alarm_mode == ALARM_OFF)
      {
        tmp_abc2_hold_reg.dose_alarm_mode = "alarm off";
      }
      else if(d_alarm_mode == ALARM_ON)
      {
        tmp_abc2_hold_reg.dose_alarm_mode = "alarm on";
      }
      else if (d_alarm_mode == ALARM_ON_EXT)
      {
        tmp_abc2_hold_reg.dose_alarm_mode = "alarm on + external";
      }
      else
      {
        tmp_abc2_hold_reg.dose_alarm_mode = "undefined value!";
      }
      //- dose alarm level (2 registers)
      unsigned short d_lsb = vholding_reg_data.at(HREG_DOSE_ALARM_LEVEL_IDX);
      unsigned short d_msb = vholding_reg_data.at(HREG_DOSE_ALARM_LEVEL_IDX+1);
      tmp_abc2_hold_reg.dose_alarm_level = ((d_lsb<<16) | d_msb)/1000.;
      //- dose alarm muted
      tmp_abc2_hold_reg.dose_alarm_muted = vholding_reg_data.at(HREG_DOSE_ALARM_MUTED_IDX);
      //- dose reset
      tmp_abc2_hold_reg.dose_reset = vholding_reg_data.at(HREG_DOSE_RESET_IDX);
      //- measurement number
      tmp_abc2_hold_reg.measurement_number = vholding_reg_data.at(HREG_NEW_MEAS_NUMBER_IDX);
      //- current time
      tmp_abc2_hold_reg.current_time = yat::Format("{02d}/{02d}/{02d} {02d}h{02d}m{02d}s{02d}").arg(vholding_reg_data[HREG_CURRENT_TIME_IDX]).arg(vholding_reg_data[HREG_CURRENT_TIME_IDX+1]).arg(vholding_reg_data[HREG_CURRENT_TIME_IDX+2]).arg(vholding_reg_data[HREG_CURRENT_TIME_IDX+3]).arg(vholding_reg_data[HREG_CURRENT_TIME_IDX+4]).arg(vholding_reg_data[HREG_CURRENT_TIME_IDX+5]).arg(vholding_reg_data[HREG_CURRENT_TIME_IDX+6]);
      //- device calibration factor
      tmp_abc2_hold_reg.dev_calibration_factor = vholding_reg_data.at(HREG_DEV_CALIB_FACTOR_IDX);
      //- vial calibration faactor
      tmp_abc2_hold_reg.vial_calibration_factor = vholding_reg_data.at(HREG_VIAL_CALIB_FACTOR_IDX);
      //- temperature threshold
      tmp_abc2_hold_reg.temperature_threshold = vholding_reg_data.at(HREG_TEMP_THRESHOLD_IDX);

      {//- copy data
        yat::AutoMutex<> guard(m_holding_reg_mutex);
        m_abc2_hold_reg = tmp_abc2_hold_reg;
      }
    }
  }
  catch(Tango::DevFailed& df)
  {
    ERROR_STREAM << "Failed to read holding registers caught [DevFailed]:\n" << df << std::endl;
    Tango::Except::print_exception(df);
  }
  catch(std::exception& e)
  {
    FATAL_STREAM << "Parsing error, description :\n" << e.what() << std::endl;
  }
  catch(...)
  {
    ERROR_STREAM << "Failed to read holding registers caught [...]." << std::endl;
  }
}

// ============================================================================
// ABC2Task::get_input_registers_data
// ============================================================================
InputRegisters ABC2Task::get_input_registers_data ()
{
  yat::AutoMutex<> guard(m_input_reg_mutex);

  return m_abc2_inp_reg;
}

// ============================================================================
// ABC2Task::update_input_registers
// ============================================================================
void ABC2Task::update_input_registers()
{
  std::vector<short> vinput_reg_data;
  InputRegisters tmp_abc2_inp_reg;
  yat::Timer t;

  try
  {
    if(m_com_p)
    {
      //- read back data in one shot
      vinput_reg_data.clear();
      vinput_reg_data = m_com_p->read_input_registers(INPUT_REGISTER_START_ADDRESS, INPUT_REGISTER_LENGTH);
    }
    //- all data received: parse data
    if(vinput_reg_data.size() == INPUT_REGISTER_LENGTH)
    {
      //- firmware version:
      char firm_v[IREG_FIRMWARE_VERSION_LGTH] = {0};
      for(size_t i=0;i<IREG_FIRMWARE_VERSION_LGTH;i++)
      {
        firm_v[i] = vinput_reg_data[i];
      }
      //- copy firmware version data
      tmp_abc2_inp_reg.firmware_version.assign(firm_v, firm_v + IREG_FIRMWARE_VERSION_LGTH);

      //- serial number
      tmp_abc2_inp_reg.serial_number = vinput_reg_data[IREG_SERIAL_NUM_IDX]<<8 | vinput_reg_data[IREG_SERIAL_NUM_IDX+1];
      
      //- vial type
      unsigned short vtype = vinput_reg_data.at(IREG_VIAL_TYPE_IDX);
      if ( !vtype )
      {
        tmp_abc2_inp_reg.vial_type = "no vial";
      }
      else
      {
        tmp_abc2_inp_reg.vial_type = "SDD-100";
      }

      //- vial serial
      std::stringstream s;
      s << "SDD-" << vinput_reg_data.at(IREG_VIAL_SERIAL_IDX) << "-" << vinput_reg_data.at(IREG_VIAL_SERIAL_IDX+1) << std::ends;
      tmp_abc2_inp_reg.vial_serial = s.str();

      //- calibration factor
      tmp_abc2_inp_reg.calibration = vinput_reg_data.at(IREG_CALIBRATION_IDX) << 8 | vinput_reg_data.at(IREG_CALIBRATION_IDX+1);

      //- initial number of bubbles
      tmp_abc2_inp_reg.initial_loading = vinput_reg_data.at(IREG_INIT_LOADING_IDX);

      //- Number of drops at test start
      tmp_abc2_inp_reg.drops_at_test_start = vinput_reg_data.at(IREG_DROP_AT_START_TEST_IDX);
      
      //- start time of measurement
      tmp_abc2_inp_reg.start_time = yat::Format("{02d}/{02d}/{02d} {02d}h{02d}m{02d}s:{02d}").arg(vinput_reg_data[IREG_START_TIME_IDX]).arg(vinput_reg_data[IREG_START_TIME_IDX+1]).arg(vinput_reg_data[IREG_START_TIME_IDX+2]).arg(vinput_reg_data[IREG_START_TIME_IDX+3]).arg(vinput_reg_data[IREG_START_TIME_IDX+4]).arg(vinput_reg_data[IREG_START_TIME_IDX+5]).arg(vinput_reg_data[IREG_START_TIME_IDX+6]);

      //- temperature
      tmp_abc2_inp_reg.temperature = vinput_reg_data.at(IREG_TEMPERATURE_IDX)/100.;

      //- battery charge
      tmp_abc2_inp_reg.battery_charge = vinput_reg_data.at(IREG_BATTERY_CHARGE_IDX);

      //- measurement number
      tmp_abc2_inp_reg.measurement_number = vinput_reg_data.at(IREG_MEAS_NUM_IDX);

      //- total dose: 2 registers
      unsigned short d_lsb = vinput_reg_data.at(IREG_TOTAL_DOSE_IDX);
      unsigned short d_msb = vinput_reg_data.at(IREG_TOTAL_DOSE_IDX+1);
      tmp_abc2_inp_reg.total_dose = ((d_lsb<<16) | d_msb)/1000.;

      //- dose since reset
      unsigned short dsr_lsb = vinput_reg_data.at(IREG_DOSE_SINCE_RESET_IDX);
      unsigned short dsr_msb = vinput_reg_data.at(IREG_DOSE_SINCE_RESET_IDX+1);
      tmp_abc2_inp_reg.dose_since_reset = ((dsr_lsb<<16) | dsr_msb)/1000.;

      //- total number of pops
      tmp_abc2_inp_reg.total_pops = vinput_reg_data.at(IREG_TOTAL_POPS_SINCE_RESET_IDX);

      //- Number of pops since dose reset
      tmp_abc2_inp_reg.pops_since_reset = vinput_reg_data.at(IREG_POPS_SINCE_RESET_IDX);

      //- remaining drops
      tmp_abc2_inp_reg.remaing_drops = vinput_reg_data.at(IREG_REMAIN_POPS_IDX);

      //- External alarm firing state
      tmp_abc2_inp_reg.external_alarm = vinput_reg_data.at(IREG_EXT_ALARM_IDX);

      //- Dose alarm tripped (0: no, >=1: yes)
      tmp_abc2_inp_reg.dose_alarm_tripped = vinput_reg_data.at(IREG_DOSE_ALARM_TRIPPED_IDX);

      //- device calibration factor
      tmp_abc2_inp_reg.dev_calibration_factor_r = vinput_reg_data.at(IREG_DEV_CALIBRATION_FACTOR_IDX);

      //- vial calibration factor
      tmp_abc2_inp_reg.vial_calibration_factor_r = vinput_reg_data.at(IREG_VIAL_CALIBRATION_FACTOR_IDX);

      //- last reset date from device is inconsistent!
      //- now update in reset command.
      // tmp_abc2_inp_reg.last_reset_date = yat::Format("{02d}/{02d}/{02d} {02d}h{02d}m{02d}s:{02d}").arg(vinput_reg_data[IREG_LAST_RESET_DATE_IDX]).arg(vinput_reg_data[IREG_LAST_RESET_DATE_IDX+1]).arg(vinput_reg_data[IREG_LAST_RESET_DATE_IDX+2]).arg(vinput_reg_data[IREG_LAST_RESET_DATE_IDX+3]).arg(vinput_reg_data[IREG_LAST_RESET_DATE_IDX+4]).arg(vinput_reg_data[IREG_LAST_RESET_DATE_IDX+5]).arg(vinput_reg_data[IREG_LAST_RESET_DATE_IDX+6]);
      // std::cout << "Reset done at: " << tmp_abc2_inp_reg.last_reset_date << std::endl;

      //- date of vial reset
      tmp_abc2_inp_reg.last_vial_reset_date = yat::Format("{02d}/{02d}/{02d} {02d}h{02d}m").arg(vinput_reg_data[IREG_LAST_VIAL_RESET_DATE_IDX]).arg(vinput_reg_data[IREG_LAST_VIAL_RESET_DATE_IDX+1]).arg(vinput_reg_data[IREG_LAST_VIAL_RESET_DATE_IDX+2]).arg(vinput_reg_data[IREG_LAST_VIAL_RESET_DATE_IDX+3]).arg(vinput_reg_data[IREG_LAST_VIAL_RESET_DATE_IDX+4]);

      //- firmware version
      char disp_v[IREG_DISPLAY_VERSION_LGTH] = {0};
      for(size_t d=IREG_DISPLAY_VERSION_IDX,n=0;d<IREG_DISPLAY_VERSION_IDX+IREG_DISPLAY_VERSION_LGTH;d++,n++)
      {
        disp_v[n] = vinput_reg_data[d];
      }
      tmp_abc2_inp_reg.display_version.assign(disp_v, disp_v+IREG_DISPLAY_VERSION_LGTH);
      
      //- state of the temperature alarm
      tmp_abc2_inp_reg.temperature_warning_state = vinput_reg_data.at(IREG_TEMP_WARN_STATE_IDX);

      //- vial alarm state
      tmp_abc2_inp_reg.vial_alarm_state = vinput_reg_data.at(IREG_VIAL_ALARM_STATE_IDX);

      //- build timestamp
      yat::Timestamp startup_ts;
      _GET_TIME(startup_ts);

      {//- critical section
        yat::AutoMutex<> guard(m_input_reg_mutex);
        //- copy data
        m_abc2_inp_reg = tmp_abc2_inp_reg;
        //- update timestamp value
        _TIMESTAMP_TO_DATE(startup_ts, m_abc2_inp_reg.timestamp);
        //- convert time_since_reset seconds in hour, minutes, seconds format
        yat::Duration tsr(m_automatic_reset_timer.time_to_expiration());
        m_abc2_inp_reg.time_since_reset = tsr.to_string();
        //- update last reset date
        m_abc2_inp_reg.last_reset_date = m_last_reset_date;
      }
      //- No error: reset error counter
      m_err_cpt = 0;
    }
  }
  catch(Tango::DevFailed& df)
  {
    ERROR_STREAM << "Failed to read input registers caught [DevFailed]:\n" << df << std::endl;
    Tango::Except::print_exception(df);
    ++m_err_cpt;
  }
  catch(std::exception& e)
  {
    FATAL_STREAM << "Parsing error, description :\n" << e.what() << std::endl;
  }
  catch(...)
  {
    ERROR_STREAM << "Failed to read input registers caught [...]." << std::endl;
    ++m_err_cpt;
  }
}

// ============================================================================
// ABC2Task::get_ABC2_status
// ============================================================================
std::string ABC2Task::get_ABC2_status(Tango::DevState& state)
{
  yat::Timer t;
  std::stringstream s;
  std::string statusStr         ("");

  if (m_err_cpt > m_nb_max_retries)
  {
    state = Tango::FAULT;
    s << "Nb max consecutives READ error reached!" << std::ends;
    //- invalid data
    {
      yat::AutoMutex<> guard(m_input_reg_mutex);
      m_abc2_inp_reg.on_error = true;
    }
  }
  else if (m_nb_max_reset_retries > m_nb_max_retries)
  {
    state = Tango::FAULT;
    s << "Nb max consecutives RESET error reached!" << std::ends;
  }
  else
  {
    {
      yat::AutoMutex<> guard(m_input_reg_mutex);
      m_abc2_inp_reg.on_error = false;
    }
    state = Tango::ON;
    s << "No error." << std::endl;
  }

  return s.str();
}

// ============================================================================
// ABC2Task::unlock_reset_cmd
// ============================================================================
void ABC2Task::unlock_reset_cmd()
{
  bool waitable = true;

  //- instanciate a message of type kDOUBLE_SCALAR_MSG
  yat::Message* msg = new yat::Message(kUNLOCK_CMDs_MSG, DEFAULT_MSG_PRIORITY, waitable);
  if ( !msg )
  {
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "ABC2Task::unlock_reset_cmd");
  }

  //- in case you want to wait for the msg to be handled before returning (synchronous approach)
  //- just do the following...
  wait_msg_handled(msg, 2000);
}

// ============================================================================
// ABC2Task::reset
// ============================================================================
void ABC2Task::reset()
{
  //- check the command has been unlocked
  if ( !m_reset_cmd_unlocked )
  {
    Tango::Except::throw_exception ("COMMAND_NOT_ALLOWED",
                                    "Reset command is locked!",
                                    "ABC2Task::reset");
  }

  //- instanciate a message
  yat::Message* msg = new yat::Message(kRESET_CMD, DEFAULT_MSG_PRIORITY);
  if ( !msg )
  {
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "ABC2Task::reset");
  }

  post(msg);
}

//-----------------------------------------------------------------------
//
//                            INTERNAL METHODS
//
//-----------------------------------------------------------------------
// ============================================================================
// ABC2Task::init_communication
// ============================================================================
void ABC2Task::init_communication()
{
  try
  {
    if ( !m_proxy_device_name.empty() )
    {
      //- Creates the proxy
      m_com_p.reset(new Communication(m_host_device, m_proxy_device_name) );
      m_com_p->create_proxy();
    }
  }
  catch(std::bad_alloc& bd)
  {
    FATAL_STREAM << "ABC2Task::init_communication() : OUT_OF_MEMORY unable to create proxy on : "
                 << m_proxy_device_name
                 << std::endl;
    FATAL_STREAM << "Description :\n" << bd.what() << std::endl;
    close_communication();
  }
  catch(Tango::DevFailed& df)
  {
    close_communication();
    Tango::Except::print_exception(df);
  }
  catch(...)
  {
    FATAL_STREAM << "ABC2Task::init_communication() : unable to create proxy, caught [...]. " << endl;
    close_communication();
  }
}

// ============================================================================
// ABC2Task::close_communication
// ============================================================================
void ABC2Task::close_communication()
{
  m_com_p.reset();
}

//-----------------------------------------------------------------------
//
//                           RESET MANAGEMENT
//
//-----------------------------------------------------------------------
void ABC2Task::initialize_4h_reset_timer()
{
  //- set unit in seconds
  m_automatic_reset_timer.set_unit(yat::Timeout::TimeoutUnit::TMO_UNIT_SEC);
  //- get the time in seconds till the next reset
  yat::CurrentTime ct;
  double ct_secs = ct.hour() * 3600. + ct.minute() * 60. + ct.second();
  double fourHours_inSec = k4_HOURS_RESET_VALUE;

  double time_to_next_reset = fourHours_inSec - ( ::fmod(ct_secs, fourHours_inSec) );

  //- configure reset timer with the computed value
  m_automatic_reset_timer.set_value(time_to_next_reset);
  //- start the reset timer timeout to reset automatically the ABC2
  m_automatic_reset_timer.restart();
}

// ============================================================================
// ABC2Task::unlock_cmds_i
// ============================================================================
void ABC2Task::unlock_cmds_i ()
{
  //- compare returns 0 if strings are equals
  if ( !m_pwd_in_clear.compare(m_pwd_decrypted) )
  {
    m_reset_cmd_unlocked = true;
  }
}

// ============================================================================
// ABC2Task::reset_i
// ============================================================================
void ABC2Task::reset_i ()
{
  try
  {
    if(m_com_p)
    {
      m_com_p->write_holding_register(HOLDING_REGISTERS_START_ADDRESS+HREG_DOSE_RESET_IDX, ENABLE_RESET);
      //- TODO: sleep a few?
      m_com_p->write_holding_register(HOLDING_REGISTERS_START_ADDRESS+HREG_DOSE_RESET_IDX, DISABLE_RESET);
      //- reset ok (no retry!)
      m_reset_err_cpt = 0;
    }

    //- update last reset date value
    yat::Timestamp lrd_ts;
    _GET_TIME(lrd_ts);

    {//- copy in critical section
      yat::AutoMutex<> guard(m_input_reg_mutex);
      _TIMESTAMP_TO_DATE(lrd_ts, m_last_reset_date);
      INFO_STREAM << "\nReset done at: " << m_last_reset_date << "\n" << std::endl;
    }
  }
  catch(Tango::DevFailed& df)
  {
    ERROR_STREAM << "Failed to send reset cmd caught [DevFailed]:\n" << df << std::endl;
    ++m_reset_err_cpt;
  }
  catch(...)
  {
    ERROR_STREAM << "Failed to send reset cmd caught [...]." << std::endl;
    ++m_reset_err_cpt;
  }
}

// ============================================================================
// ABC2Task::decrypt_codes
// ============================================================================
void ABC2Task::decrypt_codes ()
{
  char l;
  std::string mdpResult("");

  for (unsigned long int idx = 0; idx < m_codes_list.size(); ++idx)
  {
    l = decrypt(m_codes_list.at(idx));
    mdpResult += l;
  }

  m_pwd_decrypted = mdpResult;
}

/****************************FONCTION DECRYPTAGE*******************************/
unsigned char ABC2Task::decrypt(unsigned long int k)
{
  unsigned long int i,res = 1;
  unsigned char x;

  for(i = 0; i < m_kd; i++)
  {
    res = res * k;
    res = fmod((double)res,(double)m_kn);
  }

  x = ((unsigned char)res);

  return x;
}

} // namespace
