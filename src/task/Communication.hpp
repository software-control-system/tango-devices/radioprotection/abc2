// ============================================================================
//
// = CONTEXT
//    TANGO Project - Communication Class
//
// = FILENAME
//    Communication.hpp
//
// = AUTHOR
//    X. Elattaoui
// ============================================================================

#ifndef _COM_CLASS_H_
#define _COM_CLASS_H_

#include <tango.h>
#include <string>
#include <DeviceProxyHelper.h>
#include <yat4tango/LogHelper.h>

/**
 *  \brief Class to manage communication link
 *
 *  \author Xavier Elattaoui
 *  \date 07-2022
 */
namespace ABC2_ns
{

class Communication : yat4tango::TangoLogAdapter
{
public :
  typedef enum {
    NOT_DEFINED,
    SERIAL,
    ETHERNET
  } communication_protocol;
  
  /**
  *  \brief Initialization.
  */
  Communication (Tango::DeviceImpl * host_device,
                 std::string com_device_name);

  /**
  *  \brief Release resources.
  */
  virtual ~Communication ();

  /**
  *  \brief Sends command to the controller and returns the read back response.
  */
  std::vector<short> read_input_registers(short reg_addr, short reg_lgth);

  std::vector<short> read_holding_registers(short reg_addr, short reg_lgth);
  void write_holding_register(short reg_addr, short val);
  
  /**
  *  \brief Proxy creation.
  */
  void create_proxy();
  
  /**
  *  \brief Checks proxy creation.
  */
  bool is_proxy_ok()
  {
  	return m_proxy ? true : false;
  }
  
  /**
  *  \brief Returns communication errors if any.
  */
  std::string get_com_error()
  {
  	return m_error;
  }
  
  Tango::DevState get_com_state()
  {
    return m_com_state;
  }
  
  /**
  *  \brief Clears errors.
  */
  void clear_error();

protected :

 
private :
  
  void delete_proxy();
  
  void check_proxy();

  //- proxy on Modbus device
  yat::SharedPtr<Tango::DeviceProxyHelper> m_proxy;

  //- the host device
  Tango::DeviceImpl*        m_host_dev;
  
  //- communication device name
  std::string               m_dev_name;

  //- errors
  std::string 		          m_error;
  
  //- communication state
  Tango::DevState           m_com_state;
  
};

} //- end namespace

#endif // _COM_CLASS_H_

