

//-----------------------------------------------------------------------------
//-                             INPUT REGISTERS
//-----------------------------------------------------------------------------
const std::size_t INPUT_REGISTER_START_ADDRESS = 30001;
const std::size_t INPUT_REGISTER_LENGTH        = 56;
//-----------------------------------------------------------------------------
//- Input registers data indexes
const std::size_t IREG_FIRMWARE_VERSION_IDX       = 0;
const std::size_t IREG_FIRMWARE_VERSION_LGTH      = 6;
const std::size_t IREG_SERIAL_NUM_IDX             = 6;
const std::size_t IREG_SERIAL_NUM_LGTH            = 2;
const std::size_t IREG_VIAL_TYPE_IDX              = 8;
const std::size_t IREG_VIAL_SERIAL_IDX            = 9;
const std::size_t IREG_VIAL_SERIAL_LGTH           = 2;
const std::size_t IREG_CALIBRATION_IDX            = 11;
const std::size_t IREG_CALIBRATION_LGTH           = 2;
const std::size_t IREG_INIT_LOADING_IDX           = 13;
const std::size_t IREG_DROP_AT_START_TEST_IDX     = 14;
const std::size_t IREG_START_TIME_IDX             = 15;
const std::size_t IREG_START_TIME_LGTH            = 7;
const std::size_t IREG_TEMPERATURE_IDX            = 22;
const std::size_t IREG_BATTERY_CHARGE_IDX         = 23;
const std::size_t IREG_MEAS_NUM_IDX               = 24;
const std::size_t IREG_TOTAL_DOSE_IDX             = 25;
const std::size_t IREG_TOTAL_DOSE_LGTH            = 2;
const std::size_t IREG_DOSE_SINCE_RESET_IDX       = 27;
const std::size_t IREG_DOSE_SINCE_RESET_LGTH      = 2;
const std::size_t IREG_TOTAL_POPS_SINCE_RESET_IDX = 29;
const std::size_t IREG_POPS_SINCE_RESET_IDX       = 30;
const std::size_t IREG_REMAIN_POPS_IDX            = 31;
const std::size_t IREG_EXT_ALARM_IDX              = 32;
const std::size_t IREG_DOSE_ALARM_TRIPPED_IDX     = 33;
const std::size_t IREG_DEV_CALIBRATION_FACTOR_IDX = 34;
const std::size_t IREG_VIAL_CALIBRATION_FACTOR_IDX= 35;
const std::size_t IREG_LAST_RESET_DATE_IDX        = 36;
const std::size_t IREG_LAST_RESET_DATE_LGTH       = 7;
const std::size_t IREG_LAST_VIAL_RESET_DATE_IDX   = 43;
const std::size_t IREG_LAST_VIAL_RESET_DATE_LGTH  = 5;
const std::size_t IREG_DISPLAY_VERSION_IDX        = 48;
const std::size_t IREG_DISPLAY_VERSION_LGTH       = 6;
const std::size_t IREG_TEMP_WARN_STATE_IDX        = 54;
const std::size_t IREG_VIAL_ALARM_STATE_IDX       = 55;
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//-                             HOLDING REGISTERS
//-----------------------------------------------------------------------------
const std::size_t HOLDING_REGISTERS_START_ADDRESS = 20001;
const std::size_t HOLDING_REGISTERS_LENGTH        = 17;
//- Holding registers data indexes
const std::size_t HREG_DOSE_ALARM_MODE_IDX        = 0;
const std::size_t HREG_DOSE_ALARM_LEVEL_IDX       = 1;
const std::size_t HREG_DOSE_ALARM_LEVEL_LGTH      = 2;
const std::size_t HREG_DOSE_ALARM_MUTED_IDX       = 4;
const std::size_t HREG_DOSE_RESET_IDX             = 5;
const std::size_t HREG_NEW_MEAS_NUMBER_IDX        = 6;
const std::size_t HREG_CURRENT_TIME_IDX           = 7;
const std::size_t HREG_CURRENT_TIME_LGTH          = 7;
const std::size_t HREG_DEV_CALIB_FACTOR_IDX       = 14;
const std::size_t HREG_VIAL_CALIB_FACTOR_IDX      = 15;
const std::size_t HREG_TEMP_THRESHOLD_IDX         = 16;
//-----------------------------------------------------------------------------
//- reset command values
const short ENABLE_RESET  = 1;
const short DISABLE_RESET = 0;
//-----------------------------------------------------------------------------



