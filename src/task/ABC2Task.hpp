// ============================================================================
//
// = CONTEXT
//		ABC2Task
//
// = File
//		ABC2Task.hpp
//
// = AUTHOR
//		X. Elattaoui Synchrotron Soleil France
//
// ============================================================================

#ifndef _ABC2_TASK_H_
#define _ABC2_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <vector>
#include <yat/memory/SharedPtr.h>
#include <yat/threading/Atomic.h>
#include <yat4tango/DeviceTask.h>
#include <yat/time/Timer.h>        //- timer to lock Reset cmd
#include <DeviceProxyHelper.h>
#include "ABC2Data.hpp"
#include "Communication.hpp"

/**
 *  ABC2Task class description:
 *    This class reads dose from the ABC2 through a Modbus protocol.
 *    No commands Start, Stop needed.
 */

namespace ABC2_ns
{
// ============================================================================
// class: ABC2Task
// ============================================================================
class ABC2Task : public yat4tango::DeviceTask
{
public:
	//- ctor ---------------------------------
  ABC2Task (Tango::DeviceImpl * host_device, 
            std::string ds_proxy_name,
            std::size_t nb_max_retries);

	//- dtor ---------------------------------
	virtual ~ABC2Task (void);

	//- getters ------------------------------
  
  HoldingRegisters get_holding_registers_data ();
  InputRegisters get_input_registers_data ();

  //- State/status management
  std::string get_ABC2_status(Tango::DevState&);

  //- RESET protection management
  inline void set_password(const std::string& pwd)
  {
    m_pwd_in_clear = pwd;
  }

  inline void set_n (unsigned long int n)
  {
    m_kn = n;
  }

  inline void set_d (unsigned long int d)
  {
    m_kd = d;
  }

  inline void set_codesList (std::vector<unsigned long int>& codesList)
  {
    m_codes_list.assign(codesList.begin(), codesList.end());
  }

	//- reset management
  void unlock_reset_cmd();

  void reset();

  inline bool is_reset_cmd_unlocked ()
  {
    return m_reset_cmd_unlocked;
  }

protected:

  //- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message (yat::Message& msg) throw (Tango::DevFailed);

  //- internal mutex
  yat::Mutex m_holding_reg_mutex;
  yat::Mutex m_input_reg_mutex;
  yat::Mutex m_err_cpt_mutex;

private:
  void init_communication();
  void close_communication();

  //- HW data : updated at init (read only once)
  void update_input_registers ();
  void update_holding_registers ();
 
  //- 4h RESET management
  void initialize_4h_reset_timer();	//- done at init
	void reset_i			 ();
	void unlock_cmds_i ();
  void decrypt_codes ();
  unsigned char decrypt(unsigned long int k);
  //- is command available
  bool m_reset_cmd_unlocked;
  //- password
  std::string m_pwd_in_clear;
  std::string m_pwd_decrypted;
  //- keys
  unsigned long int m_kn;
  unsigned long int m_kd;
  //- codes list to decrypt
  std::vector<unsigned long int> m_codes_list;
	//- timer to lock reset cmd after kLOCK_CMDS_AFTER seconds
  yat::Timer m_unlock_cmd_timer;
  //- timeout timer to send the reset cmd periodically each 4 hours
  yat::Timeout m_automatic_reset_timer;
  //- updated after each command sent
  std::string m_last_reset_date;

  //- communication link proxy
  Tango::DeviceImpl*	 	m_host_device;
  std::string           m_proxy_device_name;
  yat::SharedPtr<Communication> m_com_p;

  //- structure used to share data between device and task
	InputRegisters   m_abc2_inp_reg;
  HoldingRegisters m_abc2_hold_reg;

	std::string m_battery_charge;

  //- ERRORS and RETRIES management
  //- com error counter
  yat::Atomic<std::size_t> m_err_cpt;
  //- reset eroor counter
  yat::Atomic<std::size_t> m_reset_err_cpt;

  //- max consecutives com error before device switch to ALARM state
  std::size_t m_nb_max_retries;

  //- retry to send reset cmd on error
  std::size_t m_nb_max_reset_retries;
};

} // namespace

#endif // _ABC2_TASK_H_
