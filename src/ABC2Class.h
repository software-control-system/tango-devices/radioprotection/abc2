/*----- PROTECTED REGION ID(ABC2Class.h) ENABLED START -----*/
//=============================================================================
//
// file :        ABC2Class.h
//
// description : Include for the ABC2 root class.
//               This class is the singleton class for
//                the ABC2 device class.
//               It contains all properties and methods which the 
//               ABC2 requires only once e.g. the commands.
//
// project :     ABC2 Neutron Dosimeter
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef ABC2Class_H
#define ABC2Class_H

#include "ABC2.h"


/*----- PROTECTED REGION END -----*/	//	ABC2Class.h


namespace ABC2_ns
{
/*----- PROTECTED REGION ID(ABC2Class::classes for dynamic creation) ENABLED START -----*/


/*----- PROTECTED REGION END -----*/	//	ABC2Class::classes for dynamic creation

//=========================================
//	Define classes for attributes
//=========================================
//	Attribute TotalDose class definition
class TotalDoseAttrib: public Tango::Attr
{
public:
	TotalDoseAttrib():Attr("TotalDose",
			Tango::DEV_DOUBLE, Tango::READ) {};
	~TotalDoseAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_TotalDose(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_TotalDose_allowed(ty);}
};

//	Attribute TotalCounts class definition
class TotalCountsAttrib: public Tango::Attr
{
public:
	TotalCountsAttrib():Attr("TotalCounts",
			Tango::DEV_DOUBLE, Tango::READ) {};
	~TotalCountsAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_TotalCounts(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_TotalCounts_allowed(ty);}
};

//	Attribute DoseSinceReset class definition
class DoseSinceResetAttrib: public Tango::Attr
{
public:
	DoseSinceResetAttrib():Attr("DoseSinceReset",
			Tango::DEV_DOUBLE, Tango::READ) {};
	~DoseSinceResetAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_DoseSinceReset(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_DoseSinceReset_allowed(ty);}
};

//	Attribute CountsSinceReset class definition
class CountsSinceResetAttrib: public Tango::Attr
{
public:
	CountsSinceResetAttrib():Attr("CountsSinceReset",
			Tango::DEV_DOUBLE, Tango::READ) {};
	~CountsSinceResetAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_CountsSinceReset(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_CountsSinceReset_allowed(ty);}
};

//	Attribute TimeSinceReset class definition
class TimeSinceResetAttrib: public Tango::Attr
{
public:
	TimeSinceResetAttrib():Attr("TimeSinceReset",
			Tango::DEV_STRING, Tango::READ) {};
	~TimeSinceResetAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_TimeSinceReset(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_TimeSinceReset_allowed(ty);}
};

//	Attribute Timestamp class definition
class TimestampAttrib: public Tango::Attr
{
public:
	TimestampAttrib():Attr("Timestamp",
			Tango::DEV_STRING, Tango::READ) {};
	~TimestampAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_Timestamp(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_Timestamp_allowed(ty);}
};

//	Attribute ExternalAlarm class definition
class ExternalAlarmAttrib: public Tango::Attr
{
public:
	ExternalAlarmAttrib():Attr("ExternalAlarm",
			Tango::DEV_BOOLEAN, Tango::READ) {};
	~ExternalAlarmAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_ExternalAlarm(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_ExternalAlarm_allowed(ty);}
};

//	Attribute Temperature class definition
class TemperatureAttrib: public Tango::Attr
{
public:
	TemperatureAttrib():Attr("Temperature",
			Tango::DEV_DOUBLE, Tango::READ) {};
	~TemperatureAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_Temperature(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_Temperature_allowed(ty);}
};

//	Attribute LastResetDate class definition
class LastResetDateAttrib: public Tango::Attr
{
public:
	LastResetDateAttrib():Attr("LastResetDate",
			Tango::DEV_STRING, Tango::READ) {};
	~LastResetDateAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_LastResetDate(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_LastResetDate_allowed(ty);}
};

//	Attribute LastVialResetDate class definition
class LastVialResetDateAttrib: public Tango::Attr
{
public:
	LastVialResetDateAttrib():Attr("LastVialResetDate",
			Tango::DEV_STRING, Tango::READ) {};
	~LastVialResetDateAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_LastVialResetDate(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_LastVialResetDate_allowed(ty);}
};

//	Attribute RemainingDrops class definition
class RemainingDropsAttrib: public Tango::Attr
{
public:
	RemainingDropsAttrib():Attr("RemainingDrops",
			Tango::DEV_DOUBLE, Tango::READ) {};
	~RemainingDropsAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_RemainingDrops(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_RemainingDrops_allowed(ty);}
};

//	Attribute doseAlarmMode class definition
class doseAlarmModeAttrib: public Tango::Attr
{
public:
	doseAlarmModeAttrib():Attr("doseAlarmMode",
			Tango::DEV_STRING, Tango::READ) {};
	~doseAlarmModeAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_doseAlarmMode(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_doseAlarmMode_allowed(ty);}
};

//	Attribute doseAlarmThreshold class definition
class doseAlarmThresholdAttrib: public Tango::Attr
{
public:
	doseAlarmThresholdAttrib():Attr("doseAlarmThreshold",
			Tango::DEV_DOUBLE, Tango::READ) {};
	~doseAlarmThresholdAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_doseAlarmThreshold(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_doseAlarmThreshold_allowed(ty);}
};

//	Attribute currentTime class definition
class currentTimeAttrib: public Tango::Attr
{
public:
	currentTimeAttrib():Attr("currentTime",
			Tango::DEV_STRING, Tango::READ) {};
	~currentTimeAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_currentTime(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_currentTime_allowed(ty);}
};

//	Attribute deviceCalibrationFactor class definition
class deviceCalibrationFactorAttrib: public Tango::Attr
{
public:
	deviceCalibrationFactorAttrib():Attr("deviceCalibrationFactor",
			Tango::DEV_DOUBLE, Tango::READ) {};
	~deviceCalibrationFactorAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_deviceCalibrationFactor(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_deviceCalibrationFactor_allowed(ty);}
};

//	Attribute vialCalibrationFactor class definition
class vialCalibrationFactorAttrib: public Tango::Attr
{
public:
	vialCalibrationFactorAttrib():Attr("vialCalibrationFactor",
			Tango::DEV_DOUBLE, Tango::READ) {};
	~vialCalibrationFactorAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_vialCalibrationFactor(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_vialCalibrationFactor_allowed(ty);}
};

//	Attribute temperatureThreshold class definition
class temperatureThresholdAttrib: public Tango::Attr
{
public:
	temperatureThresholdAttrib():Attr("temperatureThreshold",
			Tango::DEV_DOUBLE, Tango::READ) {};
	~temperatureThresholdAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_temperatureThreshold(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_temperatureThreshold_allowed(ty);}
};

//	Attribute temperatureWarningState class definition
class temperatureWarningStateAttrib: public Tango::Attr
{
public:
	temperatureWarningStateAttrib():Attr("temperatureWarningState",
			Tango::DEV_BOOLEAN, Tango::READ) {};
	~temperatureWarningStateAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_temperatureWarningState(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_temperatureWarningState_allowed(ty);}
};

//	Attribute serialNumber class definition
class serialNumberAttrib: public Tango::Attr
{
public:
	serialNumberAttrib():Attr("serialNumber",
			Tango::DEV_ULONG, Tango::READ) {};
	~serialNumberAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_serialNumber(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_serialNumber_allowed(ty);}
};

//	Attribute firmwareVersion class definition
class firmwareVersionAttrib: public Tango::Attr
{
public:
	firmwareVersionAttrib():Attr("firmwareVersion",
			Tango::DEV_STRING, Tango::READ) {};
	~firmwareVersionAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_firmwareVersion(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_firmwareVersion_allowed(ty);}
};

//	Attribute vialSerial class definition
class vialSerialAttrib: public Tango::Attr
{
public:
	vialSerialAttrib():Attr("vialSerial",
			Tango::DEV_STRING, Tango::READ) {};
	~vialSerialAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<ABC2 *>(dev))->read_vialSerial(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<ABC2 *>(dev))->is_vialSerial_allowed(ty);}
};


//=========================================
//	Define classes for commands
//=========================================
//	Command Reset class definition
class ResetClass : public Tango::Command
{
public:
	ResetClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	ResetClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~ResetClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<ABC2 *>(dev))->is_Reset_allowed(any);}
};

//	Command UnlockCmd class definition
class UnlockCmdClass : public Tango::Command
{
public:
	UnlockCmdClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	UnlockCmdClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~UnlockCmdClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<ABC2 *>(dev))->is_UnlockCmd_allowed(any);}
};


/**
 *	The ABC2Class singleton definition
 */

#ifdef _TG_WINDOWS_
class __declspec(dllexport)  ABC2Class : public Tango::DeviceClass
#else
class ABC2Class : public Tango::DeviceClass
#endif
{
	/*----- PROTECTED REGION ID(ABC2Class::Additionnal DServer data members) ENABLED START -----*/
	
	
	/*----- PROTECTED REGION END -----*/	//	ABC2Class::Additionnal DServer data members

	public:
		//	write class properties data members
		Tango::DbData	cl_prop;
		Tango::DbData	cl_def_prop;
		Tango::DbData	dev_def_prop;
	
		//	Method prototypes
		static ABC2Class *init(const char *);
		static ABC2Class *instance();
		~ABC2Class();
		Tango::DbDatum	get_class_property(string &);
		Tango::DbDatum	get_default_device_property(string &);
		Tango::DbDatum	get_default_class_property(string &);
	
	protected:
		ABC2Class(string &);
		static ABC2Class *_instance;
		void command_factory();
		void attribute_factory(vector<Tango::Attr *> &);
		void pipe_factory();
		void write_class_property();
		void set_default_property();
		void get_class_property();
		string get_cvstag();
		string get_cvsroot();
	
	private:
		void device_factory(const Tango::DevVarStringArray *);
		void create_static_attribute_list(vector<Tango::Attr *> &);
		void erase_dynamic_attributes(const Tango::DevVarStringArray *,vector<Tango::Attr *> &);
		vector<string>	defaultAttList;
		Tango::Attr *get_attr_object_by_name(vector<Tango::Attr *> &att_list, string attname);
};

}	//	End of namespace

#endif   //	ABC2_H
